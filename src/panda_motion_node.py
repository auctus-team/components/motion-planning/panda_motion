#!/usr/bin/env python3
import rospy
from panda_motion import PandaMotion
from panda_motion.srv import PoseSrv, GetPose


def GoToCallback(req):
    panda.stop()
    panda.goToPose(req.pose, blocking=req.blocking)
    return True


def GoToRelativeCallback(pose, frame_id):
    # TO DO : Custom message with blocking term + ADD Rotation
    panda.stop()
    panda.goToPoseRelative(
        x=pose.position.x, y=pose.position.y, z=pose.position.z, blocking=True
    )
    return True


def GetPandaPoseCallback(req):
    return True, panda.getCurrentPose()


if __name__ == "__main__":
    rospy.wait_for_service("/panda/get_planning_scene")

    panda = PandaMotion("/panda/robot_description", "panda", "panda_arm")

    GoTo_Service = rospy.Service("/panda/panda_motion/goto", PoseSrv, GoToCallback)
    GoToRel_Service = rospy.Service(
        "/panda/panda_motion/gotorel", PoseSrv, GoToRelativeCallback
    )
    GetPandaPose_Service = rospy.Service(
        "/panda/panda_motion/getPose", GetPose, GetPandaPoseCallback
    )
    loop_rate = rospy.Rate(100)
    # spin since ros is okay
    while not rospy.is_shutdown():
        loop_rate.sleep()

    panda.stop()
