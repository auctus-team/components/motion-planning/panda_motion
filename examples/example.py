from panda_motion import PandaMotion
import time
from geometry_msgs.msg import Pose
import rospy
import numpy as np
import copy
import math
from scipy.spatial.transform import Rotation as R
import signal 


def signal_handler(sig, frame):
    print("You pressed Ctrl+C, Quiting!")
    sys.exit(0)


signal.signal(signal.SIGINT, signal_handler)


def lemniscate():
    center_pose = panda.getCurrentPose()
    
    r = 0.2
    waypoints = []
    t = np.linspace(0, 2*np.pi, num=100)
    alpha = 0.1
    x = alpha * np.sqrt(2) * np.cos(t) * np.sin(t) / (np.sin(t)**2 + 1)
    y = alpha * np.sqrt(2) * np.cos(t) / (np.sin(t)**2 + 1)
    z = 0.1 * np.sin(t)
    next_target_pose = copy.deepcopy(center_pose)
    q_curr = R.from_quat([next_target_pose.orientation.x, next_target_pose.orientation.y, next_target_pose.orientation.z, next_target_pose.orientation.w])
    for t_i in range(0,len(t)):
        r_x = R.from_rotvec(math.sin(t[t_i]) * np.array([1, 0, 0 ]))
        r_y = R.from_rotvec(math.sin(t[t_i]) * np.array([0, 1, 0 ]))
        next_target_pose = copy.deepcopy(center_pose)
        next_target_pose.position.x += x[t_i] 
        next_target_pose.position.y += y[t_i]
        next_target_pose.position.z += z[t_i]
        new_q_curr = r_y* r_x * q_curr
        next_target_pose.orientation.x = new_q_curr.as_quat()[0]
        next_target_pose.orientation.y = new_q_curr.as_quat()[1]
        next_target_pose.orientation.z = new_q_curr.as_quat()[2]
        next_target_pose.orientation.w  = new_q_curr.as_quat()[3]
        
        waypoints.append(copy.deepcopy(next_target_pose))
    return waypoints


if __name__ == "__main__":
    panda = PandaMotion("/panda/robot_description", "panda", "panda_arm",load_gripper=True)

    rospy.loginfo("Going Home")
    panda.goHome()


    rospy.loginfo("Go To Pose Relative")
    panda.goToPoseRelative(x=-0.1, y=0.2)

    panda.goHome()
    rospy.loginfo("Same motion but at 0.3 m/s ")
    panda.setCartesianVelocityLimits(0.3)
    panda.goToPoseRelative(x=-0.1, y=0.2)

    panda.setCartesianVelocityLimits(0.1)
    target_pose = panda.getCurrentPose()
    target_pose.position.x += 0.1
    rospy.loginfo("Go To Pose")
    panda.goToPose(target_pose)

    rospy.loginfo("Go To TF object")
    panda.goToTF("object")

    panda.goHome()

    panda.goToPoseRelative(x=0.3,z=-0.2)

    waypoints = lemniscate()
    rospy.loginfo("Doing a lemniscate")
    panda.goToWaypoints(waypoints)


    panda.setJointAccelerationScalingFactor(0.8)
    panda.setJointVelocityScalingFactor(0.25)
    panda.goHome()

    joint_goal = panda.getMoveGroupCommander().get_current_joint_values()
    joint_goal[0] = 0
    joint_goal[1] = -2.0 * math.pi / 8
    joint_goal[2] = 0
    joint_goal[3] = -2.0 * math.pi / 4
    joint_goal[4] = 0
    joint_goal[5] = 2.0 * math.pi / 6  # 1/6 of a turn
    joint_goal[6] = 0
    panda.goToJointConfiguration(joint_goal)

    panda.goHome()

    pick_approach = panda.getCurrentPose()
    pick_approach.position.x = 0.6
    pick_approach.position.y = 0.0
    pick_approach.position.z = 0.04 # A bit above

    pick_pose = panda.getCurrentPose()
    pick_pose.position.x = 0.6
    pick_pose.position.y = 0.0
    pick_pose.position.z = 0.02 # At the center

    openning_goal = panda.gripper.create_opening_goal(0.08,0.1)
    closing_goal = panda.gripper.create_closing_goal(width=0.005,speed=0.1,force=30)

    panda.pick(pick_approach,pick_pose,openning_goal,closing_goal)

    place_approach = copy.deepcopy(pick_approach)
    place_approach.position.y +=0.1
    
    place_pose = copy.deepcopy(pick_pose)
    place_pose.position.y +=0.1

    panda.place(pick_approach,place_approach,place_pose,openning_goal)