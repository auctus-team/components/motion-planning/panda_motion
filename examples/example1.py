import panda_motion
import time
from geometry_msgs.msg import Pose

if __name__ == "__main__":
    panda = panda_motion.PandaMotion("/panda/robot_description", "panda", "panda_arm")

    panda.goHome()
    time.sleep(1)
    panda.goToPoseRelative(x=-0.1, y=0.2)
    time.sleep(1)
    target_pose = panda.getCurrentPose()
    target_pose.position.x += 0.1
    panda.goToPose(target_pose)
