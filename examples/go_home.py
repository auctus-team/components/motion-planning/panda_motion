from panda_motion import PandaMotion
import time
import rospy
import sys

if __name__ == "__main__":
    rospy.logwarn("GoHome")
    rospy.logwarn(sys.argv)
    if len(sys.argv) < 4:
        print("usage: go_home.py /panda/robot_description panda panda_arm False")
    if sys.argv[4] == "false":
        gripper = False
    else:
        gripper = True
    panda = PandaMotion(sys.argv[1], sys.argv[2], sys.argv[3],load_gripper=gripper)

    rospy.loginfo("Going Home")
    panda.goHome()


    