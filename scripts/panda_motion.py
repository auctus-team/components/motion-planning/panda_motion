#!/usr/bin/env python3
import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import actionlib
from franka_gripper.msg import (
    MoveGoal,
    MoveAction,
    GraspGoal,
    GraspAction,
    GraspEpsilon,
)
from tf2_ros import Buffer, TransformListener
from geometry_msgs.msg import Pose, WrenchStamped
import tf
import panda_gripper


class PandaMotion():

    def __init__(
        self,
        robot_description_param,
        ns,
        group_name,
        load_gripper=False,
        display_trajectory=True,
        init_node=True,
    ):
        super(PandaMotion, self).__init__()
        ## First initialize `moveit_commander`_ and a `rospy`_ node:
        moveit_commander.roscpp_initialize(sys.argv)
        if init_node:
            rospy.init_node("panda_motion", anonymous=True)
        # rospy.wait_for_service('/panda/velocity_qp/getNewCartesianPath')

        ## Instantiate a `RobotCommander`_ object. This object is the outer-level interface to
        ## the robot:
        self.robot = moveit_commander.RobotCommander(robot_description_param, ns=ns)

        ## Instantiate a `PlanningSceneInterface`_ object.  This object is an interface
        ## to the world surrounding the robot:
        self.scene = moveit_commander.PlanningSceneInterface(ns=ns)

        ## Instantiate a `MoveGroupCommander`_ object.  This object is an interface
        ## to one group of joints.  In this case the group is the joints in the Panda
        ## arm so we set ``group_name = panda_arm``. If you are using a different robot,
        ## you should change this value to the name of your romanipulatorning group.
        ## This interface can be used to plan and execute motions on the Panda:
        self.group = moveit_commander.MoveGroupCommander(
            group_name, robot_description=robot_description_param, ns=ns
        )

        self.check_force = False
        rospy.Subscriber(
            ns + "/franka_state_controller/F_ext",
            WrenchStamped,
            self.externalWrenchCallback,
        )

        self.tfBuffer = Buffer()
        tfListener = TransformListener(self.tfBuffer)

        self.setCartesianVelocityLimits(0.1)
        self.setJointVelocityScalingFactor(0.1)
        self.setJointAccelerationScalingFactor(0.1)

        self.display_trajectory = display_trajectory

        if self.display_trajectory:
            self.display_trajectory_publisher = rospy.Publisher(
                "/move_group/display_planned_path",
                moveit_msgs.msg.DisplayTrajectory,
                queue_size=20,
            )

        if load_gripper:
            self.gripper = panda_gripper.PandaGripper(ns)

    def goToJointConfiguration(self,target_joint_configuration,blocking=True):
        print("target_joint_configuration")
        print(target_joint_configuration)
        plan = self.planJointSpace(target_joint_configuration)
        self.go(plan, blocking)

    def planJointSpace(self, target_joint_configuration):
        print("plantJointSpace target_joint_configuration")
        print(target_joint_configuration)
        self.group.set_joint_value_target(target_joint_configuration)
        return self.group.plan()[1]

    def planCartesianSpace(self,waypoint, blocking=True):
        plan, ratio = self.group.compute_cartesian_path(waypoint, 0.01)
        if ratio < 1.0:
            self.planJointSpace(waypoint[1])
        else:
            self.go(plan, blocking)

    def goToPose(self, target_pose, blocking=True):
        self.group.clear_pose_targets()
        current_pose = self.group.get_current_pose().pose
        waypoint = []
        # We need to define a waypoint to define linear trajectory
        waypoint.append(current_pose)

        waypoint.append(target_pose)

        self.planCartesianSpace(waypoint, blocking)

    def getTfPose(self, tip_name, root_name="world"):
        tf = self.tfBuffer.lookup_transform(
            root_name, tip_name, rospy.Time(0), rospy.Duration(20)
        )
        tf_pose = Pose(tf.transform.translation, tf.transform.rotation)
        return tf_pose

    def getCurrentPose(self):
        return self.group.get_current_pose().pose

    def setCartesianVelocityLimits(self, max_velocity):
        self.group.limit_max_cartesian_link_speed(
            max_velocity, self.group.get_end_effector_link()
        )

    def setJointVelocityScalingFactor(self, max_velocity_scaling_factor):
        self.group.set_max_velocity_scaling_factor(max_velocity_scaling_factor)

    def setJointAccelerationScalingFactor(self, max_acceleration_scaling_factor):
        self.group.set_max_acceleration_scaling_factor(max_acceleration_scaling_factor)

    def goToTF(self, tip_name, root_name="world", blocking=True):
        target_pose = self.getTfPose(tip_name, root_name)
        return self.goToPose(target_pose, blocking)

    def goToPoseRelative(
        self, x=0, y=0, z=0, rot_x=0, rot_y=0, rot_z=0, constraints=None, blocking=True
    ):
        """
        It takes in a relative position and orientation, and moves the robot to that position.

        :param x: The x coordinate of the pose to move to, defaults to 0 (optional)
        :param y: 0.0, defaults to 0 (optional)
        :param z: The height of the end effector above the ground, defaults to 0 (optional)
        :param rot_x: rotation around the x axis, defaults to 0 (optional)
        :param rot_y: rotation around the y axis, defaults to 0 (optional)
        :param rot_z: rotation around the z axis, defaults to 0 (optional)
        :param cartesian: True if you want a linear trajectory, False if you want a non-linear trajectory,
        defaults to True (optional)
        """

        self.group.clear_pose_targets()
        current_pose = self.group.get_current_pose().pose
        waypoint = []
        # We need to define a waypoint to define linear trajectory
        waypoint.append(current_pose)
        relative_pose = copy.deepcopy(current_pose)

        relative_pose.position.x += x
        relative_pose.position.y += y
        relative_pose.position.z += z

        # Force gripper to be parallele to floor
        quat_relative = tf.transformations.quaternion_from_euler(rot_x, rot_y, rot_z)
        quat_current = (
            current_pose.orientation.x,
            current_pose.orientation.y,
            current_pose.orientation.z,
            current_pose.orientation.w,
        )

        quat = tf.transformations.quaternion_multiply(quat_current, quat_relative)
        relative_pose.orientation.x = quat[0]
        relative_pose.orientation.y = quat[1]
        relative_pose.orientation.z = quat[2]
        relative_pose.orientation.w = quat[3]

        waypoint.append(relative_pose)

        return self.planCartesianSpace(waypoint, blocking)

    def pick(
        self, approach_pose, target_pose, openning_goal, closing_goal, waiting_time=5.0
    ):
        self.goToPose(approach_pose)
        self.gripper.open(openning_goal, waiting_time)
        self.goToPose(target_pose)
        self.gripper.close(closing_goal, waiting_time)
        self.goToPose(approach_pose)

    def place(self, approach_pose, target_pose, openning_goal, waiting_time=5.0):
        self.goToPose(approach_pose)
        self.goToPose(target_pose)
        self.gripper.open(openning_goal, waiting_time)
        # self.goToPose(approach_pose)

    def goToWaypoints(self, waypoints, blocking=True):
        [plan, ratio] = self.group.compute_cartesian_path(waypoints, 0.01)
        ret = self.group.execute(plan, wait=blocking)
        return ret

    def go(self, plan, blocking=True):
        if self.display_trajectory:
            self.displayTrajectory(plan)
        ret = self.group.execute(plan, wait=blocking)
        return ret

    def goHome(self):
        self.group.set_named_target("home")
        plan = self.group.plan()[1]
        self.go(plan, blocking=True)

    def stop(self):
        self.group.stop()

    def setMaxEffort(self, max_effort):
        self.max_effort = max_effort
        self.check_force = True

    def setPathConstraint(self, path_constraint):
        # TODO set path constraint
        rospy.logwarn("Not implemented yet")

    def externalWrenchCallback(self, data):
        current_effort = data.wrench
        # TODO Find a good way to compare the efforts
        if self.check_force == True and current_effort > self.max_effort:
            self.stop()

    def getMoveGroupCommander(self):
        return self.group

    def getRobotCommander(self):
        return self.robot

    def getPlanningScene(self):
        return self.scene

    def displayTrajectory(self, plan):
        display_trajectory = moveit_msgs.msg.DisplayTrajectory()
        display_trajectory.trajectory_start = self.robot.get_current_state()
        display_trajectory.trajectory.append(plan)
        # Publish
        self.display_trajectory_publisher.publish(display_trajectory)
