import rospy
import actionlib
from franka_gripper.msg import (
    MoveGoal,
    MoveAction,
    GraspGoal,
    GraspAction,
    GraspEpsilon,
)


class PandaGripper:
    def __init__(self, ns):
        self.gripper_move_action_client = actionlib.SimpleActionClient(
            "/panda/franka_gripper/move", MoveAction
        )
        self.gripper_move_action_client.wait_for_server()
        self.gripper_grasp_action_client = actionlib.SimpleActionClient(
            "/panda/franka_gripper/grasp", GraspAction
        )
        self.gripper_grasp_action_client.wait_for_server()

    def open(self, move_goal, waiting_time=5.0):
        rospy.logdebug(
            "Opening gripper of %d m at a speed of %d m/s",
            move_goal.width,
            move_goal.speed,
        )
        self.gripper_move_action_client.send_goal(move_goal)
        self.gripper_move_action_client.wait_for_result(
            rospy.Duration.from_sec(waiting_time)
        )
        rospy.logdebug("Gripper opened")
        return self.gripper_move_action_client.get_result().success

    def close(self, closing_goal, waiting_time=5.0):
        rospy.logdebug(
            "Closing gripper with at %d m at a speed of %d m/s and a force of %d",
            closing_goal.width,
            closing_goal.speed,
            closing_goal.force,
        )

        self.gripper_grasp_action_client.send_goal(closing_goal)
        self.gripper_grasp_action_client.wait_for_result(
            rospy.Duration.from_sec(waiting_time)
        )
        rospy.loginfo("Gripper closed")
        return self.gripper_grasp_action_client.get_result().success

    def create_closing_goal(self, width, speed, force, inner=0.1, outer=0.1):
        goal = GraspGoal()
        goal.width = width
        goal.speed = speed
        goal.force = force
        goal.epsilon = GraspEpsilon(inner=inner, outer=outer)
        return goal

    def create_opening_goal(self, width, speed):
        return MoveGoal(width=width, speed=speed)

    # TODO Homing function
